<?php

namespace Drupal\comment_mover\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\comment_mover\Clipboard;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Comment Mover form.
 */
class ClipboardBlockForm extends FormBase {

  /**
   * Construct the clipboard form class.
   *
   * @param \Drupal\comment_mover\Clipboard $clipboard
   *   The comment mover clipboard service.
   */
  public function __construct(protected readonly Clipboard $clipboard) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('comment_mover.clipboard'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'comment_mover_clipboard_block';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear clipboard'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->clipboard->clear();
  }

}
