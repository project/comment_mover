<?php

namespace Drupal\comment_mover\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\comment_mover\Clipboard;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Comment Mover routes.
 */
class CommentMoverController extends ControllerBase {

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\comment_mover\Clipboard $clipboard
   *   The Comment Mover Clipboard service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entity_type_manager,
    protected Clipboard $clipboard,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('comment_mover.clipboard')
    );
  }

  /**
   * Cut an entity to the comment clipboard.
   *
   * @param string $entity_type
   *   The type of entity.
   * @param int $entity_id
   *   The entity identifier.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request to get the return destination.
   */
  public function cut(string $entity_type, int $entity_id, Request $request) {
    $this->clipboard->cut($entity_type, $entity_id);

    return new RedirectResponse($request->get('destination'));
  }

  /**
   * Paste comments from the clipboard under the specified entity.
   *
   * @param string $entity_type
   *   The type of entity.
   * @param int $entity_id
   *   The entity identifier.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request to get the return destination.
   */
  public function paste(string $entity_type, int $entity_id, Request $request) {
    $this->clipboard->paste($entity_type, $entity_id);
    return new RedirectResponse($request->get('destination'));
  }

}
