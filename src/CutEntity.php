<?php

namespace Drupal\comment_mover;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Class to store a cut entity on the clipboard.
 */
class CutEntity {

  /**
   * Constructor to set up a clipboard entry.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The type of entity on the clipboard.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $title
   *   The title of the entity.
   * @param string $author
   *   The author of the entity.
   */
  public function __construct(
    public readonly EntityTypeInterface $entityType,
    public readonly EntityInterface $entity,
    public readonly string $title,
    public readonly string $author,
  ) {
  }

}
