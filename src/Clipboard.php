<?php

namespace Drupal\comment_mover;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Service description.
 */
class Clipboard {

  use MessengerTrait, StringTranslationTrait;

  /**
   * The private storage for the clipboard.
   *
   * @var \Drupal\Core\Entity\PrivateTempStore
   */
  protected PrivateTempStore $clipboardStore;

  /**
   * Constructs a Clipboard object.
   *
   * @param \Drupal\comment_mover\CommentMover $mover
   *   The comment_mover.mover service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private user session storage area.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheInvalidator
   *   The cache invalidator service.
   */
  public function __construct(
    protected CommentMover $mover,
    protected PrivateTempStoreFactory $privateTempStoreFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected CacheTagsInvalidatorInterface $cacheInvalidator,
  ) {
    $this->clipboardStore = $privateTempStoreFactory->get('comment_mover');
  }

  /**
   * Check if clipboard is empty.
   *
   * @return bool
   *   True if no entries in clipboard.
   */
  public function isEmpty(): bool {
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    return count($cutEntities) == 0;
  }

  /**
   * Clear any cut entities from the clipboard.
   */
  public function clear(): void {
    $this->clipboardStore->set('clipboard', []);
    // Invalidate the clipboard block.
    $this->cacheInvalidator->invalidateTags(['comment_mover:clipboard']);
  }

  /**
   * Remove an entity from the clipboard.
   *
   * @param string $entity_type
   *   The type of entity being cut.
   * @param int $entity_id
   *   The identifier of the entity to cut.
   */
  public function remove(string $entity_type, int $entity_id): void {
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    foreach ($cutEntities as $key => $item) {
      /** @var \Drupal\comment_mover\CutEntity $item */
      if ($entity_type == $item->entityType->id() && $item->entity->id()) {
        unset($cutEntities[$key]);
        $this->clipboardStore->set('clipboard', $cutEntities);
        return;
      }
    }
    // Invalidate the clipboard block.
    $this->cacheInvalidator->invalidateTags(['comment_mover:clipboard']);
  }

  /**
   * Get the number of cut items on the clipboard.
   *
   * @return int
   *   The number of comments on the clipboard.
   */
  public function length(): int {
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    return count($cutEntities);
  }

  /**
   * Check if an entity is already on the clipboard.
   *
   * @param string $entity_type
   *   The type of entity being cut.
   * @param int $entity_id
   *   The identifier of the entity to cut.
   *
   * @return bool
   *   True if clipboard contains the entity.
   */
  public function contains(string $entity_type, int $entity_id): bool {
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    /** @var CutEntity $item */
    foreach ($cutEntities as $item) {
      if ($entity_type == $item->entityType->id() && $entity_id == $item->entity->id()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Return contents of clipboard for display in block.
   *
   * @return array
   *   Two dimensional array containing cut entities.
   */
  public function show(): array {
    $clipboardItems = [];
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    foreach ($cutEntities as $item) {
      $clipboardItems[] = [
        /** @var \Drupal\comment_mover\CutEntity $item */
        'title' => $item->title,
        'author' => $item->author,
      ];
    }
    return $clipboardItems;
  }

  /**
   * Cut a comment or entity to the clipboard.
   *
   * @param string $entity_type
   *   The type of entity being cut.
   * @param int $entity_id
   *   The identifier of the entity to cut.
   */
  public function cut(string $entity_type, int $entity_id): void {
    // Check that comment isn't on clipboard already.
    if ($this->contains($entity_type, $entity_id)) {
      $this->messenger()->addError($this->t("You cannot cut this comment because it is already on the clipboard."));
      return;
    }
    $entityStorage = $this->entityTypeManager->getStorage($entity_type);
    $entity = $entityStorage->load($entity_id);
    // Check no children in clipboard already.
    if (!$this->checkChildrenNotInClipboard($entity)) {
      $this->messenger()->addError($this->t("You cannot cut this comment because there is already a child comment in the clipboard. Please paste the child, or remove from comments before cutting this comment."));
      return;
    }
    // Also checked no ancestor comments in clipboard.
    if (!$this->checkAncestorsNotInClipboard($entity)) {
      $this->messenger()->addError($this->t("You cannot cut this comment because there is already an ancestor comment in the clipboard. Please paste or empty the clipboard first."));
      return;
    }
    if ($entity_type == 'comment') {
      /** @var \Drupal\comment\Entity\Comment $entity */
      $title = $entity->getSubject();
      $author = $entity->getAuthorName();
    }
    else {
      // @todo Handle other entity types.
      $title = '';
      $author = '';
    }
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    $cutEntities[] = new CutEntity($entity->getEntityType(), $entity, $title, $author);
    $this->clipboardStore->set('clipboard', $cutEntities);
    $this->messenger()->addStatus($this->t('The @entityType has been added to the clipboard.', ['@entityType' => $entity_type]));
    // Invalidate the clipboard block.
    $this->cacheInvalidator->invalidateTags(['comment_mover:clipboard']);
  }

  /**
   * Paste entities in clipboard under specified entity.
   *
   * @param string $entity_type
   *   The type of entity being cut.
   * @param int $entity_id
   *   The identifier of the entity to cut.
   */
  public function paste(string $entity_type, int $entity_id): void {
    $entityStorage = $this->entityTypeManager->getStorage($entity_type);
    $newParent = $entityStorage->load($entity_id);
    if ($this->checkParentNotPastingToChild($newParent)) {
      $this->messenger()->addError($this->t("You cannot paste under this @entity because there is an ancestor of it in the clipboard."));
      return;
    }
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    $numPasted = 0;
    /** @var CutEntity $item */
    foreach ($cutEntities as $item) {
      $this->mover->moveComment($item->entity, $newParent);
      $this->remove($item->entityType->id(), $item->entity->id());
      $numPasted++;
    }
    $this->messenger()->addStatus($this->t('You have pasted @pasted comment(s).', ['@pasted' => $numPasted]));
    // Invalidate the clipboard block.
    $this->cacheInvalidator->invalidateTags(['comment_mover:clipboard']);
  }

  /**
   * Check no child comments of the entity are in the clipboard.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   True if no children of entity found in clipboard.
   */
  protected function checkChildrenNotInClipboard(EntityInterface $entity): bool {
    $storage = $this->entityTypeManager->getStorage('comment');
    $query = $storage->getQuery();
    if ($entity->getEntityTypeId() == 'comment') {
      /** @var \Drupal\comment\Entity\Comment $entity */
      $results = $query->condition('entity_id', $entity->getCommentedEntityId())
        ->condition('pid', $entity->id())
        ->accessCheck(FALSE)
        ->execute();
    }
    else {
      $results = $query->condition('entity_id', $entity->id())
        ->condition('pid', NULL, 'IS NULL')
        ->accessCheck(FALSE)
        ->execute();
    }
    foreach ($storage->loadMultiple($results) as $comment) {
      // If comment exists in clipboard, or one of its children does...
      if ($this->contains($comment->getEntityTypeId(), $comment->id()) || !$this->checkChildrenNotInClipboard($comment)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Check no ancestor comments of the entity are in the clipboard.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   True if no children of entity found in clipboard.
   */
  protected function checkAncestorsNotInClipboard(EntityInterface $entity): bool {
    if ($entity->getEntityTypeId() == 'comment') {
      /** @var \Drupal\comment\Entity\Comment $entity */
      $parent = $entity->getParentComment();
      if (is_null($parent)) {
        // If no parent comment, check the owning entity.
        return $this->checkAncestorsNotInClipboard($entity->getCommentedEntity());
      }
      if ($this->contains($parent->getEntityTypeId(), $parent->id())) {
        return FALSE;
      }
      return $this->checkAncestorsNotInClipboard($parent);
    }
    // Entity not a comment, so check if it is in clipboard.
    return !$this->contains($entity->getEntityTypeId(), $entity->id());
  }

  /**
   * Check no comments in clipboard are children of the entity being pasted to.
   *
   * @param \Drupal\Core\Entity\EntityInterface $parentEntity
   *   The entity to check for children.
   *
   * @return bool
   *   True if no clipboard comments are parents of the entity to paste under.
   */
  protected function checkParentNotPastingToChild(EntityInterface $parentEntity): bool {
    $cutEntities = $this->clipboardStore->get('clipboard') ?? [];
    /** @var CutEntity $item */
    foreach ($cutEntities as $item) {
      // Check if new parent is an ancestor of the comment pasting.
      if ($this->ancestorOf($parentEntity, $item->entity)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Check no comments in clipboard are children of the entity being pasted to.
   *
   * @param \Drupal\Core\Entity\EntityInterface $parsingEntity
   *   The entity we are checking ancestors of.
   * @param \Drupal\Core\Entity\EntityInterface $searchEntity
   *   The entity being searched for.
   *
   * @return bool
   *   True if no clipboard comments are parents of the entity to paste under.
   */
  protected function ancestorOf(EntityInterface $parsingEntity, EntityInterface $searchEntity): bool {
    // Could be two objects for the same entity, so check by type and id.
    if ($parsingEntity->getEntityTypeId() == $searchEntity->getEntityTypeId() && $parsingEntity->id() == $searchEntity->id()) {
      return TRUE;
    }
    if ($parsingEntity->getEntityTypeId() == 'comment') {
      /** @var \Drupal\comment\Entity\Comment $parsingEntity */
      $parentComment = $parsingEntity->getParentComment();
      if (is_null($parentComment)) {
        return $this->ancestorOf($parsingEntity->getCommentedEntity(), $searchEntity);
      }
      return $this->ancestorOf($parentComment, $searchEntity);
    }
    // If not a comment, no parents to search, so not found.
    return FALSE;
  }

}
