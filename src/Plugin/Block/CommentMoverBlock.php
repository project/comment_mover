<?php

namespace Drupal\comment_mover\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\comment_mover\Clipboard;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "comment_mover_block",
 *   admin_label = @Translation("Comment Mover"),
 *   category = @Translation("Comment Mover")
 * )
 */
class CommentMoverBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the comment mover block.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\comment_mover\Clipboard $clipboard
   *   The comment mover clipboard service.
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
    protected Clipboard $clipboard,
    protected FormBuilder $formBuilder,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('comment_mover.clipboard'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $numComments = $this->clipboard->length();
    switch ($numComments) {
      case 0:
        $commentsText = $this->t('The clipboard is empty.');
        break;

      case 1:
        $commentsText = $this->t('There is one comment on the clipboard.');
        break;

      default:
        $commentsText = $this->t('There are @number comments on the clipboard.', ['@number' => $numComments]);
    }
    $items = array_map(
      fn($item) => $this->t(
        '%title by @author',
        ['%title' => $item['title'], '@author' => $item['author']]
      ),
      $this->clipboard->show()
    );
    $build['content'] = [
      '#markup' => $commentsText,
      'cut_items' => [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $items,
      ],
    ];
    $build['form'] = $this->formBuilder->getForm('Drupal\comment_mover\Form\ClipboardBlockForm');
    $build['#cache'] = [
      'max-age' => Cache::PERMANENT,
      'tags' => ['comment_mover:clipboard'],
      'contexts' => ['user'],
    ];
    return $build;
  }

}
