<?php

namespace Drupal\comment_mover;

use Drupal\Component\Utility\Number;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Comment Mover Service.
 */
class CommentMover {

  /**
   * Constructor for comment mover service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Move a comment to a new parent.
   *
   * @param \Drupal\Core\Entity\EntityInterface $movingEntity
   *   The moving entity.
   * @param \Drupal\Core\Entity\EntityInterface $newParentEntity
   *   The entity that the comment will belong to.
   */
  public function moveComment(EntityInterface $movingEntity, EntityInterface $newParentEntity) {
    if ($newParentEntity->getEntityTypeId() == 'comment') {
      /** @var \Drupal\comment\CommentInterface $newParentEntity */
      $newParentEntityId = $newParentEntity->getCommentedEntityId();
      $newParentCommentId = $newParentEntity->id();
      $newThread = $this->getNextChildThread($newParentEntityId, $newParentCommentId);
    }
    else {
      $newParentEntityId = $newParentEntity->id();
      $newParentCommentId = NULL;
      $newThread = $this->getNextTopLevelThread($newParentEntityId);
    }

    // @todo We assume a comment, but need to allow other entity types.
    /** @var \Drupal\comment\CommentInterface $movingEntity */
    $movingEntity->set('entity_id', $newParentEntityId);
    $movingEntity->set('pid', $newParentCommentId);
    $movingEntity->setThread($newThread);
    $movingEntity->save();

    // Update child comments.
    $this->recursivelyUpdateCommentThread($movingEntity->id(), $newParentEntityId, $newThread);
  }

  /**
   * Get the next thread value for top level comments.
   *
   * @param int $entityId
   *   The entity to place the comment under.
   *
   * @return string
   *   The next top level thread entry.
   */
  protected function getNextTopLevelThread(int $entityId): string {
    $storage = $this->entityTypeManager->getStorage('comment');
    $query = $storage->getQuery();
    $results = $query->condition('entity_id', $entityId)
      ->condition('pid', NULL, 'IS NULL')
      ->accessCheck(FALSE)
      ->execute();
    $max = rtrim((string) array_reduce($storage->loadMultiple($results), fn($max, $item) => max($max, $item->getThread())), '/');
    $num = Number::alphadecimalToInt($max);

    return Number::intToAlphadecimal(++$num) . '/';
  }

  /**
   * Get the next thread value in the sequence for a child of a parent comment.
   *
   * @param int $entityId
   *   The entity ID.
   * @param int $commentId
   *   The comment ID.
   *
   * @return string
   *   The calculated thread value.
   */
  protected function getNextChildThread(int $entityId, int $commentId): string {
    $storage = $this->entityTypeManager->getStorage('comment');
    $query = $storage->getQuery();
    $results = $query->condition('entity_id', $entityId)
      ->condition('pid', $commentId)
      ->accessCheck(FALSE)
      ->execute();
    // If no existing comments under parent, get parent's thread and append new
    // level.
    if (!count($results)) {
      /** @var Drupal\comment\Entity\Comment $comment */
      $comment = $storage->load($commentId);
      $threadBase = rtrim($comment->getThread(), '/');
      return $threadBase . '.00/';
    }
    // Comment will have siblings. Get the maximum thread, split into elements,
    // and increment the last one.
    $max = rtrim((string) array_reduce($storage->loadMultiple($results), fn($max, $item) => max($max, $item->getThread())), '/');
    $levels = explode('.', $max);
    $last = count($levels) - 1;
    $num = Number::alphadecimalToInt($levels[$last]);
    $levels[$last] = Number::intToAlphadecimal(++$num);

    return implode('.', $levels) . '/';
  }

  /**
   * Recursively loop through comments in thread, updating entity ID and thread.
   *
   * @param int $parentCommentId
   *   The comment to update children for.
   * @param int $newEntityId
   *   The entity ID the comments will belong to.
   * @param string $threadBase
   *   The thread string of the parent comment.
   */
  protected function recursivelyUpdateCommentThread(int $parentCommentId, int $newEntityId, string $threadBase): void {
    $storage = $this->entityTypeManager->getStorage('comment');
    $query = $storage->getQuery();
    $results = $query->condition('pid', $parentCommentId)
      ->accessCheck(FALSE)
      ->execute();
    $num = 0;
    foreach ($storage->loadMultiple($results) as $comment) {
      /** @var \Drupal\comment\CommentInterface $comment */
      $comment->set('entity_id', $newEntityId);
      $newThread = rtrim($threadBase, '/') . '.' . Number::intToAlphadecimal($num++) . '/';
      $comment->setThread($newThread);
      $comment->save();
      $this->recursivelyUpdateCommentThread($comment->id(), $newEntityId, $threadBase);
    }
  }

}
