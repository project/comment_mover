<?php

namespace Drupal\Tests\comment_mover\Traits;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\comment\Entity\Comment;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Build nodes and comments for testing.
 *
 * @group comment_mover
 */
trait TestCommentsTrait {

  use ContentTypeCreationTrait, NodeCreationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * A node with a comment thread.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected Node $node1;

  /**
   * A node with a single comment.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected Node $node2;

  /**
   * A node with no comments.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected Node $node3;

  /**
   * A comment with no children under node 1.
   *
   * @var \Drupal\comment\CommentInterface
   */
  protected $comment1;

  /**
   * A comment with a child comment under node 2.
   *
   * @var \Drupal\comment\CommentInterface
   */
  protected $comment2;

  /**
   * A child comment under comment 2.
   *
   * @var \Drupal\comment\CommentInterface
   */
  protected $comment2child;

  /**
   * Create nodes and comments for test of comment moving.
   */
  protected function createTestNodesAndComments(): void {
    $this->installEntitySchema('node');
    $this->installEntitySchema('comment');

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    // Create a content type.
    NodeType::create(['type' => 'article', 'name' => 'Article'])->save();

    // Add comment type.
    $this->entityTypeManager->getStorage('comment_type')->create([
      'id' => 'comment',
      'label' => 'comment',
      'target_entity_type_id' => 'node',
    ])->save();

    // Add comment field to content.
    $this->entityTypeManager->getStorage('field_storage_config')->create([
      'entity_type' => 'node',
      'field_name' => 'comment',
      'type' => 'comment',
      'settings' => [
        'comment_type' => 'comment',
      ],
    ])->save();

    /*
     * Set up nodes and comments as follows:
     *
     * - node1
     *   - comment1
     * - node2
     *   - comment2
     *     - comment2child
     * - node 3
     */

    $this->node1 = Node::create([
      'type' => 'article',
      'title' => $this->randomString(),
      'user_id' => $this->adminUser->id(),
    ]);
    $this->node1->save();

    $this->node2 = Node::create([
      'type' => 'article',
      'title' => $this->randomString(),
      'user_id' => $this->adminUser->id(),
    ]);
    $this->node2->save();

    $this->node3 = Node::create([
      'type' => 'article',
      'title' => $this->randomString(),
      'user_id' => $this->adminUser->id(),
    ]);
    $this->node3->save();

    $this->comment1 = Comment::create([
      'entity_type' => 'comment',
      'name' => $this->randomString(),
      'subject' => $this->randomString(),
      'entity_id' => $this->node1->id(),
      'comment_type' => 'comment',
      'field_name' => 'comment',
    ]);
    $this->comment1->setPublished();
    $this->comment1->save();

    $this->comment2 = Comment::create([
      'entity_type' => 'comment',
      'name' => $this->randomString(),
      'subject' => $this->randomString(),
      'entity_id' => $this->node2->id(),
      'comment_type' => 'comment',
      'field_name' => 'comment',
    ]);
    $this->comment2->setPublished();
    $this->comment2->save();

    $this->comment2child = Comment::create([
      'entity_type' => 'comment',
      'name' => $this->randomString(),
      'subject' => $this->randomString(),
      'entity_id' => $this->node2->id(),
      'pid' => $this->comment2->id(),
      'comment_type' => 'comment',
      'field_name' => 'comment',
    ]);
    $this->comment2child->setPublished();
    $this->comment2child->save();

  }

}
