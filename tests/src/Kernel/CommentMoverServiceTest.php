<?php

namespace Drupal\Tests\comment_mover\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\comment_mover\Traits\TestCommentsTrait;
use Drupal\comment_mover\CommentMover;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Test moving comments between nodes.
 *
 * @group comment_mover
 */
class CommentMoverServiceTest extends KernelTestBase {

  use TestCommentsTrait;

  /**
   * The comment mover service.
   *
   * @var \Drupal\comment_mover\CommentMover
   */
  protected CommentMover $commentMover;

  /**
   * The admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'field',
    'field',
    'text',
    'comment',
    'comment_mover',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->commentMover = $this->container->get('comment_mover.mover');

    $this->installEntitySchema('user');

    $this->adminUser = User::create([
      'name' => $this->randomString(),
    ]);
    $this->adminUser->save();

    $this->createTestNodesAndComments();
  }

  /**
   * Test moving a comment with no children to a new node without comments.
   *
   * Moves comment1 from node1 to node3 (destination has no comments).
   */
  public function testMoveCommentToTopLevel(): void {
    $this->commentMover->moveComment($this->comment1, $this->node3);
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $movedComment */
    $movedComment = $storage->load($this->comment1->id());
    $this->assertEquals($this->node3->id(), $movedComment->getCommentedEntityId());
    $this->assertFalse($movedComment->hasParentComment());
    $this->assertEquals('01/', $movedComment->getThread());
  }

  /**
   * Test moving a comment with no children to a new node with other comments.
   *
   * Moves comment1 from node1 to node2 (destination has other comments).
   */
  public function testMoveCommentToTopLevelSibling(): void {
    $this->commentMover->moveComment($this->comment1, $this->node2);
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $movedComment */
    $movedComment = $storage->load($this->comment1->id());
    $this->assertEquals($this->node2->id(), $movedComment->getCommentedEntityId());
    $this->assertFalse($movedComment->hasParentComment());
    $this->assertEquals('02/', $movedComment->getThread());
  }

  /**
   * Test moving a comment with no children to a new bottom level.
   *
   * Moves comment1 from node1 to node2 under comment2child.
   */
  public function testMoveCommentToChildComment(): void {
    $this->commentMover->moveComment($this->comment1, $this->comment2child);
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $movedComment */
    $movedComment = $storage->load($this->comment1->id());
    $this->assertEquals($this->node2->id(), $movedComment->getCommentedEntityId());
    $this->assertTrue($movedComment->hasParentComment());
    $this->assertEquals($this->comment2child->id(), $movedComment->getParentComment()->id());
    $this->assertEquals('01.00.00/', $movedComment->getThread());
  }

  /**
   * Test moving a comment with no children to new child comment with siblings.
   *
   * Moves comment1 from node1 to node2 under comment2, after sibling
   * comment2child.
   */
  public function testMoveCommentToChildCommentSibling(): void {
    $this->commentMover->moveComment($this->comment1, $this->comment2);
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $movedComment */
    $movedComment = $storage->load($this->comment1->id());
    $this->assertEquals($this->node2->id(), $movedComment->getCommentedEntityId());
    $this->assertTrue($movedComment->hasParentComment());
    $this->assertEquals($this->comment2->id(), $movedComment->getParentComment()->id());
    $this->assertEquals('01.01/', $movedComment->getThread());
  }

  /**
   * Test moving a comment with no children to a new bottom level.
   *
   * Moves comment1 from node1 to node2 under comment2child.
   */
  public function testMoveThreadToTopLevel(): void {
    $this->commentMover->moveComment($this->comment2, $this->node3);
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $movedComment */
    $movedComment = $storage->load($this->comment2->id());
    $this->assertEquals($this->node3->id(), $movedComment->getCommentedEntityId());
    $this->assertFalse($movedComment->hasParentComment());
    $this->assertEquals('01/', $movedComment->getThread());
    /** @var Drupal\comment\Entity\Comment $movedChild */
    $movedChild = $storage->load($this->comment2child->id());
    $this->assertEquals($this->node3->id(), $movedChild->getCommentedEntityId());
    $this->assertTrue($movedChild->hasParentComment());
    $this->assertEquals($this->comment2->id(), $movedChild->getParentComment()->id());
    $this->assertEquals('01.00/', $movedChild->getThread());
  }

}
