<?php

namespace Drupal\Tests\comment_mover\Kernel;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\comment_mover\Traits\TestCommentsTrait;
use Drupal\comment_mover\Clipboard;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Test description.
 *
 * @group comment_mover
 */
class ClipboardTest extends KernelTestBase {

  use TestCommentsTrait;

  /**
   * The comment mover clipboard service.
   *
   * @var \Drupal\comment_mover\Clipboard
   */
  protected Clipboard $clipboard;

  /**
   * The admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'field',
    'field',
    'text',
    'comment',
    'comment_mover',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->clipboard = $this->container->get('comment_mover.clipboard');

    $this->installEntitySchema('user');

    $this->adminUser = User::create([
      'name' => $this->randomString(),
    ]);
    $this->adminUser->save();

    $this->createTestNodesAndComments();
  }

  /**
   * Test that clipboard isEmpty() returns when empty.
   */
  public function testClipboardIsEmpty(): void {
    $this->assertTrue($this->clipboard->isEmpty());
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    $this->assertFalse($this->clipboard->isEmpty());
    $this->clipboard->clear();
    $this->assertTrue($this->clipboard->isEmpty());
  }

  /**
   * Test that contains() returns true for items on clipboard.
   */
  public function testClipboardContains(): void {
    $this->assertFalse($this->clipboard->contains($this->comment1->getEntityTypeId(), $this->comment1->id()));
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    $this->assertTrue($this->clipboard->contains($this->comment1->getEntityTypeId(), $this->comment1->id()));
    $this->assertFalse($this->clipboard->contains($this->comment2->getEntityTypeId(), $this->comment2->id()));
  }

  /**
   * Test fetching clipboard content for block.
   */
  public function testClipboardShow(): void {
    $this->assertEquals(0, count($this->clipboard->show()));
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    $cutItems = $this->clipboard->show();
    $this->assertEquals(1, count($cutItems));
    $this->assertEquals($this->comment1->getSubject(), $cutItems[0]['title']);
    $this->assertEquals($this->comment1->getAuthorName(), $cutItems[0]['author']);
    // Add a second comment and check again.
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment2->id());
    $cutItems = $this->clipboard->show();
    $this->assertEquals(2, count($cutItems));
    $this->assertEquals($this->comment1->getSubject(), $cutItems[0]['title']);
    $this->assertEquals($this->comment1->getAuthorName(), $cutItems[0]['author']);
    $this->assertEquals($this->comment2->getSubject(), $cutItems[1]['title']);
    $this->assertEquals($this->comment2->getAuthorName(), $cutItems[1]['author']);
  }

  /**
   * Test comment can't be cut if it's already on clipboard.
   */
  public function testCannotCutIfAlreadyOnClipboard(): void {
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    $this->assertEquals(1, $this->clipboard->length());
    // Attempt to cut the same comment again.
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    // Verify still only one comment in clipboard.
    $this->assertEquals(1, $this->clipboard->length());
    // Verify error message triggered.
    $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR);
    $this->assertEquals(1, count($messages));
  }

  /**
   * Test comment cannot be cut if child comment in clipboard.
   */
  public function testCannotCutIfChildOnClipboard(): void {
    $this->clipboard->cut($this->comment2child->getEntityTypeId(), $this->comment2child->id());
    $this->assertFalse($this->clipboard->isEmpty());
    $this->assertTrue($this->clipboard->contains($this->comment2child->getEntityTypeId(), $this->comment2child->id()));
    // Check parent can't be added.
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment2->id());
    $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR);
    $this->assertEquals(1, count($messages));
    $this->assertEquals(
      'You cannot cut this comment because there is already a child comment in the clipboard. Please paste the child, or remove from comments before cutting this comment.',
      (string) $messages[0]
    );
    $this->assertFalse($this->clipboard->contains($this->comment2->getEntityTypeId(), $this->comment2->id()));
    // Check we can add a comment that's not a parent.
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    $this->assertTrue($this->clipboard->contains($this->comment1->getEntityTypeId(), $this->comment1->id()));
  }

  /**
   * Test comment cannot be cut if ancestor in clipboard.
   */
  public function testCannotCutIfAncestorOnClipboard(): void {
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment2->id());
    $this->assertFalse($this->clipboard->isEmpty());
    $this->assertTrue($this->clipboard->contains($this->comment2->getEntityTypeId(), $this->comment2->id()));
    // Check child can't be added.
    $this->clipboard->cut($this->comment2child->getEntityTypeId(), $this->comment2child->id());
    $messages = \Drupal::messenger()->messagesByType(MessengerInterface::TYPE_ERROR);
    $this->assertEquals(1, count($messages));
    $this->assertEquals(
      'You cannot cut this comment because there is already an ancestor comment in the clipboard. Please paste or empty the clipboard first.',
      (string) $messages[0]
    );
    $this->assertFalse($this->clipboard->contains($this->comment2child->getEntityTypeId(), $this->comment2child->id()));
    // Check we can add a comment that's not a parent.
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    $this->assertTrue($this->clipboard->contains($this->comment1->getEntityTypeId(), $this->comment1->id()));
  }

  /**
   * Test comment cannot be pasted under its own child.
   */
  public function testCannotPasteUnderChild(): void {
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment2->id());
    $this->clipboard->paste($this->comment2child->getEntityTypeId(), $this->comment2child->id());
    $messenger = \Drupal::messenger();
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_ERROR);
    $this->assertEquals(1, count($messages));
    $this->assertEquals(
      'You cannot paste under this @entity because there is an ancestor of it in the clipboard.',
      (string) $messages[0]
    );
    // Delete messages so we know error comes from next attempt.
    $messenger->deleteAll();
    // Check comment hasn't moved.
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $stuckComment */
    $stuckComment = $storage->load($this->comment2child->id());
    $this->assertEquals($this->node2->id(), $stuckComment->getCommentedEntityId());
    $this->assertEquals($this->comment2->id(), $stuckComment->getParentComment()->id());
    // Check that adding a second comment doesn't allow it to paste.
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment1->id());
    $this->clipboard->paste($this->comment2child->getEntityTypeId(), $this->comment2child->id());
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_ERROR);
    $this->assertEquals(1, count($messages));
    $messenger->deleteAll();
    // Check comment still hasn't moved.
    /** @var Drupal\comment\Entity\Comment $stuckComment */
    $stuckComment = $storage->load($this->comment2child->id());
    $this->assertEquals($this->node2->id(), $stuckComment->getCommentedEntityId());
    $this->assertEquals($this->comment2->id(), $stuckComment->getParentComment()->id());
    // Let's clear the clipboard and cut the other comment first and try again.
    $this->clipboard->clear();
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment1->id());
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment2->id());
    $this->clipboard->paste($this->comment2child->getEntityTypeId(), $this->comment2child->id());
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_ERROR);
    $this->assertEquals(1, count($messages));
    // Final check that comment hasn't moved.
    /** @var Drupal\comment\Entity\Comment $stuckComment */
    $stuckComment = $storage->load($this->comment2child->id());
    $this->assertEquals($this->node2->id(), $stuckComment->getCommentedEntityId());
    $this->assertEquals($this->comment2->id(), $stuckComment->getParentComment()->id());
  }

  /**
   * Test comment can't be pasted under itself.
   */
  public function testCannotPasteUnderSelf(): void {
    $this->clipboard->cut($this->comment2->getEntityTypeId(), $this->comment2->id());
    $this->clipboard->paste($this->comment2->getEntityTypeId(), $this->comment2->id());
    $messenger = \Drupal::messenger();
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_ERROR);
    $this->assertEquals(1, count($messages));
    $this->assertEquals(
      'You cannot paste under this @entity because there is an ancestor of it in the clipboard.',
      (string) $messages[0]
    );
  }

  /**
   * Test cutting and pasting of comments under node.
   */
  public function testClipboardCutAndPasteToNode(): void {
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    // Check for status message.
    $messenger = \Drupal::messenger();
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->assertEquals(1, count($messages));
    $this->assertEquals('The comment has been added to the clipboard.', (string) $messages[0]);
    $messenger->deleteAll();
    // Now paste the comment and check status message.
    $this->clipboard->paste($this->node3->getEntityTypeId(), $this->node3->id());
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->assertEquals(1, count($messages));
    $this->assertEquals('You have pasted 1 comment(s).', (string) $messages[0]);
    // Check that comment has been updated.
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $movedComment */
    $movedComment = $storage->load($this->comment1->id());
    $this->assertEquals($this->node3->id(), $movedComment->getCommentedEntityId());
    $this->assertFalse($movedComment->hasParentComment());
    $this->assertEquals('01/', $movedComment->getThread());
    // After pasting the clipboard should be empty.
    $this->assertTrue($this->clipboard->isEmpty());
  }

  /**
   * Test cutting and pasting of comments under comment.
   */
  public function testClipboardCutAndPasteToComment(): void {
    $this->clipboard->cut($this->comment1->getEntityTypeId(), $this->comment1->id());
    // Check for status message.
    $messenger = \Drupal::messenger();
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->assertEquals(1, count($messages));
    $this->assertEquals('The comment has been added to the clipboard.', (string) $messages[0]);
    $messenger->deleteAll();
    // Now paste the comment and check status message.
    $this->clipboard->paste($this->comment2->getEntityTypeId(), $this->comment2->id());
    $messages = $messenger->messagesByType(MessengerInterface::TYPE_STATUS);
    $this->assertEquals(1, count($messages));
    $this->assertEquals('You have pasted 1 comment(s).', (string) $messages[0]);
    // Check that comment has been updated.
    $storage = $this->entityTypeManager->getStorage('comment');
    /** @var Drupal\comment\Entity\Comment $movedComment */
    $movedComment = $storage->load($this->comment1->id());
    $this->assertEquals($this->node2->id(), $movedComment->getCommentedEntityId());
    $this->assertTrue($movedComment->hasParentComment());
    $this->assertEquals($this->comment2->id(), $movedComment->getParentComment()->id());
    $this->assertEquals('01.01/', $movedComment->getThread());
    // After pasting the clipboard should be empty.
    $this->assertTrue($this->clipboard->isEmpty());
  }

}
