# Comment mover

This module enables comments to be moved to a new parent comment, or directly
under a parent entity, or to be converted to a new entity (e.g. page, article).
It also allows entities to be converted to comments.

In all cases child comments will be moved together with the parent comment or
entity.

Comments are moved via a comment clipboard. Cut and paste links are added to
entities and comments. Clicking cut will add a comment to the clipboard, and
clicking paste will move it under the entity or comment. A Comment Mover block
is available to list the current clipboard contents.

If you have comments in the clipboard and do not wish to move them, you can
either use the "Clear clipboard" command, or use the "❌" icon to remove
individual comments from the clipboard.

The module is currently in development for Drupal 9 and 10, and not all features
are complete yet.

The Drupal 7 version is still supported for bugfixes and security issues, but no
new features will be added.

Versions for older versions of Drupal are unsupported.


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- FAQ
- Credits
- Maintainers


## Requirements

This module requires no modules outside of Drupal core. Obviously the Comment
module must be enabled.

While the module supports Drupal 10 and 11, the minimum supported PHP version is
8.1. This is because developing for PHP 8 allows some significant language
improvements to be availed of. In particular, the amount of code needed for
dependency injection is significantly reduced. As Drupal 9 goes end of life
this year, and sites will need to upgrade to PHP 8.1 for Drupal 10, it
doesn't seem unreasonable to require this for this module.


## Recommended modules

- [Forum](https://www.drupal.org/project/forum) The Forum module was part of
  core until Drupal 10. It's certainly not required, but forum moderation is a
  pretty common use case for Comment Mover. If Forum is enabled, a "Paste" link
  will be added to Forum pages.
- [Flatcomments](https://www.drupal.org/project/flatcomments) This module forces
  comments into a single non-threaded mode, where all replies are directly
  attached to the parent entity. If Flatcomments is installed, Comment Mover
  will not display a Paste link on individual comments, only on the parent
  entity. If a comment with child comments is pasted, all comments will be
  pasted directly under the parent entity.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. After enabling the module, go to Administration > Structure > Blocks, choose
   a region for the block, and place the block "Comment Mover".
1. Set access permissions:
    - the 'administer comments' permission to move comments and nodes
    - the 'create _entity-type_' permission to convert a comment to _entity-type_


## FAQ

**Q: What happens if I cut a comment when there is already one on the comment
clipboard?**

**A:** Each comment you cut will be added to the clipboard, and displayed in the
clipboard block, if enabled. This is a slight change from the typical clipboard
metaphor, and there may be a configuration option added in future to allow the
behavior to be customized.

**Q: If I cut a comment and don't paste it, will it be lost?**

**A:** No, comments do not get moved until a paste link is clicked, then the
comments get moved to the new location. This is similar to how cut and paste
work in most operating system file management applications.

**Q: If I cut multiple comments, can I paste them to different destinations?**

**A:** Not at present. When the basic functionality is complete, it is hoped to
add the ability to paste a subset of comments in the clipboard.


## Credits

The following people worked as maintainers on previous versions, but are not involved
in Comment Mover for Drupal 9/10.

- Gerhard Killesreiter -
  [gerhard-killesreiter](https://www.drupal.org/u/gerhard-killesreiter) (Initial
  release)
- Heine Deelstra - [heine](https://www.drupal.org/u/heine) (Update to 4.7)
- Alan Doucette - [dragonwize](https://www.drupal.org/u/dragonwize) (Update to 6.x)
- Nikita Petrov - [nikita-petrov](https://www.drupal.org/u/nikita-petrov)
  (Rewrite for 7.x)
- Karoly Negyesi [chx](https://www.drupal.org/u/chx)
- Angie Byron [webchick](https://www.drupal.org/u/webchick)


## Maintainers

- James Shields - [lostcarpark](https://www.drupal.org/u/lostcarpark)
